# -*- coding: utf-8 -*-
from abc import *
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
import os
import re
from datetime import datetime
from random import randint

class UserBKInfo:
    """Класс инфа о пользователе в БК"""

    def __init__(self, total_summ, bets_summ, bets):
        """
        :param total_summ: Текущий основной баланс
        :param bets_summ: Сумма нерасчитаных ставок в пари
        :param bets: Список ставок
        """
        self.total_summ = total_summ
        self.bets_summ = bets_summ
        self.bets = bets

    def __str__(self):
        return "total_summ: {}\nbets_summ: {}\nbets: {}".format(self.total_summ, self.bets_summ, [str(x) for x in self.bets])

		
class BetBK:
    """Ставка в БК"""

    def __init__(self, id, dtime = None, game_name = None, bet_summ = 0, win_summ = 0, bet_koef = 0, status = 0):
        """
        :param id: Из столбца "#"
        :param dtime: Из столбца "Дата"
        :param game_name: Из столбца "Ставка в пари" нижняя строчка обычный шрифт
        :param bet_summ: Из столбца "Сумма"
        :param win_summ: Из столбца "Выплата"
        :param bet_koef: Из столбца "Коэфф."
        :param status: Статус ставки: -1 - не расчитанна, 0 - проигрыш, 1 - выигрыш
        """
        self.id = id
        self.dtime = dtime
        self.game_name = game_name
        self.bet_summ = bet_summ
        self.win_summ = win_summ
        self.bet_koef = bet_koef
        self.status = status

    def __str__(self):
        return str(vars(self))


class EmulatorBase(metaclass=ABCMeta):
    """Эмулятор ставок (базовый класс)"""

    def __init__(self, **kwargs):
        self.timeout = 20

        # основные настройки
        browser = kwargs.get('browser', 'chrome')
        headless = kwargs.get('headless', True)
        self.no_betting = kwargs.get('no_betting', False)

        # настройки прокси
        proxy_type = kwargs.get('proxy_type', None)
        proxy_host = kwargs.get('proxy_host', None)
        proxy_port = kwargs.get('proxy_port', None)
        proxy_login = kwargs.get('proxy_login', None)
        proxy_password = kwargs.get('proxy_password', None)

        self.screenshots_path = kwargs.get('proxy_type', None)

        if browser == 'chrome':
            capabilities = DesiredCapabilities.CHROME
            capabilities['pageLoadStrategy'] = 'none'
            options = ChromeOptions()
            options.add_argument('--disable-infobars')
            options.add_argument('--ignore-certificate-errors')
            options.add_argument('--window-size=1600,920')
            options.add_argument('--log-level=3')
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.add_argument('--disable-gpu')
            #options.add_argument('--blink-settings=imagesEnabled=false') # отключение картинок

            if proxy_type:
                print('--proxy-server={}'.format(
                    self.make_proxy_link(proxy_type, proxy_host, proxy_port, proxy_login, proxy_password)))
                options.add_argument('--proxy-server={}'.format(
                    self.make_proxy_link(proxy_type, proxy_host, proxy_port, proxy_login, proxy_password)))
        else:
            raise UnsupportedBrowser()

        # Можно запустить без GUI (для серверов)
        if headless:
            options.add_argument('--headless')

        # Создаём наш браузер
        if browser == 'chrome':
            self.browser = webdriver.Chrome(chrome_options=options, desired_capabilities=capabilities)
        else:
            raise UnsupportedBrowser()

    @staticmethod
    def make_proxy_link(self, type, host, port, login='', password=''):
        """Создает строку подключения прокси"""
        login_string = ''  # login_string = '{}:{}@'.format(login, password)
        if type == 'http':
            prefix_string = 'http://'
        elif type == 'ssl':
            prefix_string = 'https://'
        elif type == 'socks4':
            prefix_string = 'socks4://'
        elif type == 'socks5':
            prefix_string = 'socks5://'
        else:
            prefix_string = ''
        return '{}{}{}:{}'.format(prefix_string, login_string, host, port)

    def __del__(self):
        # Закрываем браузер
        try:
            self.browser.quit()
        except:
            pass

    def make_screenshot(self, path=None, name=None):
        """Создаёт скриншот по указанному пути"""
        try:
            if path is None:
                path = self.screenshots_path
            if name is None:
                name = str(datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f") + '-' + str(randint(1, 100)))
            path = os.path.join(path, name + '.png')
            self.browser.save_screenshot(path)
        except:
            pass

    def make_url(self, path):
        """Формирует URL для сайта"""
        return '{}{}'.format(self.url, path)

    @abstractmethod
    def open_main_page(self):
        """
        Открытие главной страницы БК
        :return:
        """
        pass

    @abstractmethod
    def login(self, login, password):
        """
        Логин в личный кабинет
        :param login:
        :param password:
        :return:
        """
        pass

    @abstractmethod
    def logout(self):
        """
        Выход из личного кабинета
        :return:
        """
        pass

    @abstractmethod
    def get_user_bk_info(self) -> UserBKInfo:
        """
        Получить информацию по пользователю
        :return:
        """
        pass

    @abstractmethod
    def do_bet_footbal_itb(self, **kwargs):
        """
        Сделать ставку на ИТБ команды футбол
        :param kwargs:
        :return:
        """
        pass

    @abstractmethod
    def do_bet_footbal_itm(self, **kwargs):
        """
        Сделать ставку на ИТМ команды футбол
        :param kwargs:
        :return:
        """
        pass

    @abstractmethod
    def do_bet_footbal_tm(self, **kwargs):
        """
        Сделать ставку на ТМ игры футбол
        :param kwargs:
        :return:
        """
        pass

    @abstractmethod
    def do_bet_footbal_tb(self, **kwargs):
        """
        Сделать ставку на ТБ игры футбол
        :param kwargs:
        :return:
        """
        pass


# -------- Исключения
class GameNotFound(Exception):
    """Исключение при невозможности найти играющие команды"""
    pass

class TotalNotFound(Exception):
    """Исключение при невозможности найти тоталы"""
    pass

class NothingToBet(Exception):
    """Исключение при невозможности сделать ставку с указанными условиями"""
    pass

class UnsupportedBrowser(Exception):
    """Исключение при неподдерживаемом браузере"""
    pass


