# -*- coding: utf-8 -*-

import traceback
import os
import re
from datetime import datetime
from time import sleep
from pprint import pprint
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.proxy import *
from emulators.common_classes import UserBKInfo, BetBK, EmulatorBase, GameNotFound, TotalNotFound, NothingToBet, UnsupportedBrowser


class EmulatorMarathonBet(EmulatorBase):
    """Эмулятор ставок"""

    def __init__(self, **kwargs):
        self.url = 'https://www.marathonbet.ru/su/'

        EmulatorBase.__init__(self, **kwargs)


    def __wait_for_element_by_text(self, text, should_be_visible=False):
        """Ждет появления элемента содержащего указанный текст
        :param text: Текст элемента
        :param should_be_visible: Если true, то элемент обязан быть видимым на странице
        """
        method = EC.visibility_of_element_located if should_be_visible else EC.presence_of_element_located
        WebDriverWait(self.browser, self.timeout).until(
            method((By.XPATH, '//*[contains(text(), "{}")]'.format(text)))
        )

    def __move_to_element(self, el):
        """Переходит к элементу"""
        self.browser.execute_script('arguments[0].scrollIntoView(true);', el)
        sleep(0.5)

    def __click_with_js(self, el, id='', xpath=''):
        """Эмулирует клик по элементу с помощью JavaScript
        :pram el: Элемент
        :param id: Если указан DOM id, ждём повления элемента по нему
        :param xpath: Если указан xpath, ждём повления элемента по нему
        """
        if id:
            self.__wait_for_element_to_be_clickable_by_id(id)
        if xpath:
            self.__wait_for_element_to_be_clickable_by_xpath(xpath)
        self.browser.execute_script('arguments[0].click();', el)

    def __set_value_by_id(self, id, value):
        """Устанавливает value элемента"""
        self.browser.execute_script("document.getElementById('{}').setAttribute('value', '{}')".format(id, value))

    def __try_clicking(self, el):
        """Пытается кликать элемент несколько раз"""
        for _ in range(8):
            try:
                self.__click_with_js(el)
                return
            except:
                pass
            sleep(0.5)
        self.__click_element(el)

    def open_main_page(self):
        self.browser.get(self.make_url('live/popular'))
        # Дожидаемся показа нужных элементов и действуем не ожидая остальных
        WebDriverWait(self.browser, self.timeout).until(
            EC.presence_of_element_located((By.ID, 'container_POPULAR_EVENTS'))
        )

    def login(self, login, password):
        """Вход на сайт
        :param login: Логин
        :param password: Пароль
        """
        self.open_main_page()
        WebDriverWait(self.browser, self.timeout).until(
            EC.presence_of_element_located((By.ID, 'auth_login'))
        )
        for _ in range(3):
            try:
                # Вводим логин
                login_input = self.browser.find_element_by_id('auth_login')
                login_input.send_keys(login)
                # Вводим пароль
                self.__set_value_by_id('auth_login_password', password)
                # Жмём Войти
                login_button = self.browser.find_element_by_css_selector('.login')
                login_button.click()
                # Ждём завершения входа
                self.__wait_for_element_by_id('header_balance')
                return
            except:
                login_input = self.browser.find_element_by_id('auth_login')
                login_input.clear()
            sleep(1)
        # Вводим логин
        login_input = self.browser.find_element_by_id('auth_login')
        login_input.send_keys(login)
        # Вводим пароль
        self.__set_value_by_id('auth_login_password', password)
        # Жмём Войти
        login_button = self.browser.find_element_by_css_selector('.login')
        login_button.click()
        # Ждём завершения входа
        self.__wait_for_element_by_id('header_balance')

    def __wait_for_element_by_id(self, id, should_be_visible=False):
        """Ждет появления элемента по ID
        :param id: DOM id
        :param should_be_visible: Если true, то элемент обязан быть видимым на странице
        """
        method = EC.visibility_of_element_located if should_be_visible else EC.presence_of_element_located
        WebDriverWait(self.browser, self.timeout).until(
            method((By.ID, id))
        )

    def __wait_for_element_by_class_name(self, class_name, should_be_visible=False):
        """Ждет появления элемента по css-классу
        :param class_name: DOM class
        :param should_be_visible: Если true, то элемент обязан быть видимым на странице
        """
        method = EC.visibility_of_element_located if should_be_visible else EC.presence_of_element_located
        WebDriverWait(self.browser, self.timeout).until(
            method((By.CLASS_NAME, class_name))
        )

    def __wait_for_element_by_xpath(self, xpath, should_be_visible=False):
        """Ждет появления элемента по xpath
        :param xpath: Xpath
        :param should_be_visible: Если true, то элемент обязан быть видимым на странице
        """
        method = EC.visibility_of_element_located if should_be_visible else EC.presence_of_element_located
        WebDriverWait(self.browser, self.timeout).until(
            method((By.XPATH, xpath))
        )

    def __wait_for_element_to_be_clickable_by_id(self, id):
        """Ждет кликабельности элемента по ID
        :param id: DOM id
        """
        WebDriverWait(self.browser, self.timeout).until(
            EC.element_to_be_clickable((By.ID, id))
        )

    def __wait_for_element_to_be_clickable_by_xpath(self, xpath):
        """Ждет кликабельности элемента по Xpath
        :param xpath: Xpath
        """
        WebDriverWait(self.browser, self.timeout).until(
            EC.element_to_be_clickable((By.XPATH, xpath))
        )

    def __click_element(self, el, id='', xpath=''):
        """Кликает по элементу
        :pram el: Элемент
        :param id: Если указан DOM id, ждём повления элемента по нему
        :param xpath: Если указан xpath, ждём повления элемента по нему
        """
        if id:
            self.__wait_for_element_to_be_clickable_by_id(id)
        if xpath:
            self.__wait_for_element_to_be_clickable_by_xpath(xpath)
        actions = webdriver.ActionChains(self.browser)
        actions.move_to_element_with_offset(el, 5, 5)
        actions.click(el)
        actions.perform()

    def go_to_live(self):
        """Переход на страницу LIVE"""
        self.browser.get(self.make_url('live/popular'))

    def open_live_menu(self, category):
        """Открывает LIVE-меню для указанного спорта
        :param category: Название спорта
        """
        xpath = '//*[@id="leftMenuPanel"]//*[contains(text(), "{}")]/ancestor::div[1]/ancestor::div[1]'.format(category)
        self.__wait_for_element_by_xpath(xpath)
        menu_item = self.browser.find_element_by_xpath(xpath)
        dropdown_button = menu_item.find_element_by_class_name('dropdown')
        dropdown_button.click()

    def open_live_game(self, team1, team2):
        """Открывает страницу матча
        :param team1: Команда 1
        :param team2: Команда 2
        """
        try:
            menu_item = self.browser.find_element_by_xpath(
                '//*[@id="leftMenuPanel"]//a[contains(text(), "{} - {}")]'.format(team1, team2))
        except NoSuchElementException:
            try:
                menu_item = self.browser.find_element_by_xpath(
                    '//*[@id="leftMenuPanel"]//a[contains(text(), "{} - {}")]'.format(team2, team1))
            except NoSuchElementException:
                raise GameNotFound('{} - {}'.format(team1, team2))
        self.__move_to_element(menu_item)
        self.__click_with_js(menu_item)
        self.__wait_for_element_by_text('Все выборы')

    def open_live_game_by_playname(self, play_name):
        """Открывает страницу матча
        :param team1: Команда 1
        :param team2: Команда 2
        """
        try:
            menu_item = self.browser.find_element_by_xpath(
                '//*[@id="leftMenuPanel"]//a[contains(text(), "{}")]'.format(play_name))
        except NoSuchElementException:
            raise GameNotFound('{}'.format(play_name))
        self.__move_to_element(menu_item)
        self.__click_with_js(menu_item)
        self.__wait_for_element_by_text('Все выборы')

    def open_totals(self):
        """Переходит на вкладку Тоталы"""
        try:
            totals_button = self.browser.find_element_by_xpath('//*[contains(text(), "Тоталы")]')
        except:
            raise TotalNotFound()
        self.__click_with_js(totals_button, False, '//*[contains(text(), "Тоталы")]')
        self.__wait_for_element_by_text('Тотал голов')
        sleep(0.5)

    def find_betting_button_game(self, container, handicap : float, min_kf : float, odd_type : str):
        """Находит кнопочку для ставок
        :param container: Элемент, в котором искать кнопки
        :param min_kf: Min Коэффициент
        """
        # Находим кнопочки для ставок
        price_items = container.find_elements_by_css_selector('[data-selection-price]')
        # Оставляем только кнопочки "Меньше\Больше"
        btn = '.%s_' % odd_type
        price_items = [x for x in price_items if btn in x.get_attribute('data-selection-key')]
        # Оставляем только кнопочки с коээфициентом > указанного
        price_items_coefficient = [x for x in price_items if float(x.get_attribute('data-selection-price')) >= min_kf]
        if len(price_items_coefficient) == 0:
            raise NothingToBet()
        pr = None
        if odd_type == 'Under':
            pr = price_items_coefficient[-1]
        elif odd_type == 'Over':
            pr = price_items_coefficient[0]
        return pr

    def find_betting_button_team(self, container, min_kf : float, odd_type : str):
        """Находит кнопочку для ставок
        :param container: Элемент, в котором искать кнопки
        :param min_kf: Min Коэффициент
        """
        # Находим кнопочки для ставок
        price_items = container.find_elements_by_css_selector('[data-selection-price]')
        # Оставляем только кнопочки "Меньше\Больше"
        btn = '.%s_' % odd_type
        price_items = [x for x in price_items if btn in x.get_attribute('data-selection-key')]
        # Оставляем только кнопочки с коээфициентом > указанного
        price_items_coefficient = [x for x in price_items if float(x.get_attribute('data-selection-price')) >= min_kf]
        if len(price_items_coefficient) == 0:
            raise NothingToBet()
        pr = None
        if odd_type == 'Under':
            pr = price_items_coefficient[-1]
        elif odd_type == 'Over':
            pr = price_items_coefficient[0]
        return pr

    def click_betting_button_team_total(self, team, min_kf, odd_type):
        """Нажимает кнопочку добавления ставки
        Если не получается, пробует ещё раз. Такое необходимо, т.к. кнопки периодически обновляются.
        :param team: Команда-лузер
        :param min_kf: Коэффициент
        """
        for _ in range(30):
            try:
                # Находим вкладку, где лежат ставки
                container = self.browser.find_element_by_xpath(
                    '//*[normalize-space(text())="Тотал голов ({})"]/ancestor::div[1]'.format(team))
                self.__move_to_element(container)
                button = self.find_betting_button_team(container, min_kf, odd_type)
                self.__move_to_element(button)
                self.__click_with_js(button)
                return
            except NothingToBet:
                raise NothingToBet
            except:
                pass
            sleep(1.5)
        container = self.browser.find_element_by_xpath(
            '//*[normalize-space(text())="Тотал голов ({})"]/ancestor::div[1]'.format(team))
        button = self.find_betting_button_team(container, min_kf, odd_type)
        self.__move_to_element(button)
        self.__click_with_js(button)

    def click_betting_button_game_total(self, min_kf : float, handicap: float, odd_type : str):
        """Нажимает кнопочку добавления ставки
        Если не получается, пробует ещё раз. Такое необходимо, т.к. кнопки периодически обновляются.
        :param team: Команда-лузер
        :param min_kf: Коэффициент
        """
        for _ in range(30):
            try:
                # Находим вкладку, где лежат ставки
                container = self.browser.find_element_by_xpath(
                    '//*[normalize-space(text())="Тотал голов"]/ancestor::div[1]')
                self.__move_to_element(container)
                button = self.find_betting_button_game(container, handicap, min_kf, odd_type)
                self.__move_to_element(button)
                self.__click_with_js(button)
                return
            except NothingToBet:
                raise NothingToBet
            except:
                pass
            sleep(1.5)
        container = self.browser.find_element_by_xpath('//*[normalize-space(text())="Тотал голов"]/ancestor::div[1]')
        button = self.find_betting_button_game(container, handicap, min_kf, odd_type)
        self.__move_to_element(button)
        self.__click_with_js(button)

    def bet_team_total(self, loser, min_kf, bet_sum, odd_type="Under"):
        """Делает ставку на лузера по коэффициенту
        :param loser: Команда-лузер
        :param min_kf: Коэффициент
        :param bet_sum: Сумма ставки
        """
        # Нажимает кнопку ставки
        self.click_betting_button_team_total(loser, min_kf, odd_type)
        # Вбиваем сумму ставки
        self.__wait_for_element_by_class_name('stake-input')
        stake_input = self.browser.find_element_by_class_name('stake-input')
        stake_input.send_keys(str(bet_sum))
        # Нажимаем кнопку создания ставки
        bet_button = self.browser.find_element_by_id('betslip_placebet_btn_id')
        if not self.no_betting:
            bet_button.click()
            self.__wait_for_element_by_id('betresult', True)

    def bet_game_total(self, min_kf : float, bet_sum : float, handicap: float, odd_type : str ="Under"):
        """Делает ставку на лузера по коэффициент
        :param loser: Команда-лузер
        :param min_kf: Коэффициент
        :param bet_sum: Сумма ставки
        """
        # Нажимает кнопку ставки
        self.click_betting_button_game_total(min_kf, handicap, odd_type)
        # Вбиваем сумму ставки
        self.__wait_for_element_by_class_name('stake-input')
        stake_input = self.browser.find_element_by_class_name('stake-input')
        stake_input.send_keys(str(bet_sum))
        # Нажимаем кнопку создания ставки
        bet_button = self.browser.find_element_by_id('betslip_placebet_btn_id')
        if not self.no_betting:
            bet_button.click()
            self.__wait_for_element_by_id('betresult', True)

    def click_betting_button_itb(self, team, min_kf):
        """Нажимает кнопочку добавления ставки
        Если не получается, пробует ещё раз. Такое необходимо, т.к. кнопки периодически обновляются.
        :param team: Команда-лузер
        :param min_kf: Коэффициент
        """
        for _ in range(30):
            try:
                # Находим вкладку, где лежат ставки
                container = self.browser.find_element_by_xpath(
                    '//*[normalize-space(text())="Тотал голов ({})"]/ancestor::div[1]'.format(team))
                self.__move_to_element(container)
                button = self.find_betting_button_team(container, min_kf, 'Over')
                self.__move_to_element(button)
                self.__click_with_js(button)
                return
            except NothingToBet:
                raise NothingToBet
            except:
                pass
            sleep(1.5)
        container = self.browser.find_element_by_xpath(
            '//*[normalize-space(text())="Тотал голов ({})"]/ancestor::div[1]'.format(team))
        button = self.find_betting_button_team(container, min_kf, 'Over')
        self.__move_to_element(button)
        self.__click_with_js(button)

    def do_bet_footbal_itb(self, **kwargs):
        """ Индивидуальный Тотал Больше (футбол) """
        category = 'Футбол'
        play_name = kwargs.pop('play_name')
        team = kwargs.pop('team')
        min_kf = kwargs.pop('min_kf', 1.4)
        bet_sum = kwargs.pop('bet_sum', 15)

        self.open_main_page()
        self.open_live_menu(category)
        self.open_live_game_by_playname(play_name=play_name)
        self.open_totals()
        self.bet_team_total(team, min_kf, bet_sum, 'Over')

    def do_bet_footbal_itm(self, **kwargs):
        """ Индивидуальный Тотал Меньше (футбол) """
        category = 'Футбол'
        play_name = kwargs.pop('play_name')
        team = kwargs.pop('team')
        min_kf = kwargs.pop('min_kf', 1.4)
        bet_sum = kwargs.pop('bet_sum', 15)

        self.open_main_page()
        self.open_live_menu(category)
        self.open_live_game_by_playname(play_name=play_name)
        self.open_totals()
        self.bet_team_total(team, min_kf, bet_sum, 'Under')

    def do_bet_footbal_tm(self, **kwargs):
        """ Тотал Меньше игры (футбол)"""
        category = 'Футбол'
        play_name = kwargs.pop('play_name')
        min_kf = kwargs.pop('min_kf', 1.4)
        bet_sum = kwargs.pop('bet_sum', 15)
        handicap = kwargs.pop('handicap')

        self.open_main_page()
        self.open_live_menu(category)
        self.open_live_game_by_playname(play_name=play_name)
        self.open_totals()
        self.bet_game_total(min_kf, bet_sum, handicap, 'Under')

    def do_bet_footbal_tb(self, **kwargs):
        """ Тотал Больше игры (футбол)"""
        category = 'Футбол'
        play_name = kwargs.pop('play_name')
        min_kf = kwargs.pop('min_kf', 1.4)
        bet_sum = kwargs.pop('bet_sum', 15)
        handicap = kwargs.pop('handicap')

        self.open_main_page()
        self.open_live_menu(category)
        self.open_live_game_by_playname(play_name=play_name)
        self.open_totals()
        self.bet_game_total(min_kf, bet_sum, handicap, 'Over')

    def get_user_bk_info(self) -> UserBKInfo:
        """Получает данные аккаунта пользователя"""
        self.browser.get(self.make_url('myaccount/myaccount.htm'))
        WebDriverWait(self.browser, self.timeout).until(
            EC.presence_of_element_located((By.CLASS_NAME, 'history-result'))
        )
        # Вытягиваем основной баланс
        total_summ_element = self.browser.find_element_by_css_selector('[data-punter-balance-value]')
        total_summ = float(re.sub('[^.0-9]', '', total_summ_element.get_attribute('data-punter-balance-value')))
        # Вытягиваем нерассчитанные ставки
        bets_summ_element = self.browser.find_element_by_xpath(
            '//*[contains(text(), "Нерассчитанные")]/ancestor::tr/*[contains(@class, "value")]')
        bets_summ = float(re.sub('[^.0-9]', '', bets_summ_element.text))
        # Вытягиваем историю пари
        bets = []
        history_results = self.browser.find_elements_by_class_name('history-result-main')
        for history_result in history_results:
            # Выплата
            result = history_result.find_element_by_class_name('result').text.strip().replace('₽ ', '')
            try:
                result = float(result)
            except:
                result = None
            # Статус ставки
            open_bet_element = history_result.find_element_by_class_name('open-bet')
            open_bet = -1
            try:
                open_bet_element.find_element_by_xpath('*[contains(@src, "win-icon")]')
                open_bet = 1
            except:
                try:
                    open_bet_element.find_element_by_xpath('*[contains(@src, "lose-icon")]')
                    open_bet = 0
                except:
                    pass
            # Добавляем запись истории
            bets.append(
                BetBK(
                    history_result.find_element_by_class_name('bet-number').text.strip(),
                    history_result.find_element_by_class_name('date').text.strip(),
                    history_result.find_element_by_class_name('bet-title').find_elements_by_tag_name('span')[
                        1].text.strip(),
                    float(re.sub('[^.0-9]', '', history_result.find_element_by_class_name('total-stake').text)),
                    result,
                    float(history_result.find_element_by_class_name('coefficient').text.strip()),
                    open_bet
                )
            )
        return UserBKInfo(total_summ, bets_summ, bets)

    def logout(self):
        """Выходит с сайта"""
        try:
            sleep(0.5)
            button = self.browser.find_element_by_id('logoutLink')
            self.__move_to_element(button)
            self.__click_with_js(button)
        except Exception as e:
            pass


