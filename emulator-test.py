# -*- coding: utf-8 -*-

import time
import traceback
from pprint import pprint
from emulators.marathonbet_ru import EmulatorMarathonBet

t = time.perf_counter()
emu = EmulatorMarathonBet(
    browser='chrome',
    headless=False,
    no_betting=True,
    proxy_type='',
    proxy_host='73.100.254.78',
    proxy_port='39723',
    proxy_login='',
    proxy_password=''
)

emu.login('kivilev', 'Mara2bar1')
#user_bk_info = emu.get_user_bk_info()
#pprint(user_bk_info)

try:
    team1 = 'Аргентина до 20'
    team2 = 'Мали до 20'
    play_name = '%s - %s' % (team1, team2)
    team = team2

    emu.do_bet_footbal_itm(play_name=play_name, team=team, min_kf=1.4, bet_sum=15)

    pprint('Ставка сделана!')
except Exception as e:
    pprint('Не удалось сделать ставку.')
    pprint(e)
    traceback.print_exc()

pprint('Время: {}'.format(time.perf_counter() - t))
input()
